import { Component } from "@angular/core";
import { NgxUiLoaderModule, NgxUiLoaderRouterModule } from 'ngx-ui-loader';
import {
  Router,
  NavigationCancel,
  NavigationStart,
  NavigationEnd,
  Event,
  NavigationError,
} from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  constructor(private router:Router) {
    this.router.events.subscribe((event: Event) => {
      this.navigationInterceptor(event);
    });
  }

  private navigationInterceptor(event: Event): void {
    if(event instanceof NavigationStart) {
      NgxUiLoaderRouterModule.forRoot({ showForeground: false });
      console.log('Started');
    }
    if(event instanceof NavigationEnd) {
      NgxUiLoaderRouterModule.forRoot({ showForeground: false });
      console.log('Ended');
    }
    if(event instanceof NavigationCancel) {
      NgxUiLoaderRouterModule.forRoot({ showForeground: false });
      console.log('Cancelled');
    }
    if(event instanceof NavigationError) {
      NgxUiLoaderRouterModule.forRoot({ showForeground: false });
      console.log('Error');
    }
  }
}
