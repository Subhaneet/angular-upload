import { ViewImage } from './view-image.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'; 

let formData = null;

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  url = 'http://localhost:8080';
  constructor(private http: HttpClient) {}

  uploadImage(event) {
    let fileList = event.target.files;
    if (fileList.length > 0) {
      let file = fileList[0];
      formData = new FormData();
      formData.append('productImage', file, file.name);
    }
  }

  uploadBtn() {
    this.http
      .post(`${this.url}/images`, formData)
      .subscribe(data => console.log('Success'), error => console.log(error));
  }
  getImages(): Observable<ViewImage[]> {
    return this.http.get<ViewImage[]>('http://localhost:8080/images');
  }

  saveNewImage() {
    this.http
      .post(`${this.url}/doodleImages`, formData)
        .subscribe(data => console.log('Success'),
          error => console.log(error));
  }

  deleteImage(id) {
    const url1 = `${this.url}/${id}`;
    return this.http.delete(url1)
      .pipe(
        catchError(this.handleError)
      )
  }

  handleError(error) {
    let errorMessage = '';
   if (error.error instanceof ErrorEvent) {
     errorMessage = `Error: ${error.error.message}`;
   } else {
     errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
   }
   window.alert(errorMessage);
   return throwError(errorMessage);
  }
}
