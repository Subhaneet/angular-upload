export class ViewImage {
    public id ?: string;
    public url ?: string;
    public products ?: string;

    constructor(_id?: string, url?: string, products?: string){
        this.id = _id;
        this.url = url;
        this.products = products;
    }
}