import { ViewerComponent } from './viewer/viewer.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadComponent } from './upload/upload.component';
import { ImageListComponent } from './image-list/image-list.component';


const routes: Routes = [
  {
    path: 'upload',
    component: UploadComponent
  },
  {
    path: 'image/:_id',
    component: ViewerComponent
  },
  {
    path: 'list',
    component: ImageListComponent
  },
  {
    path: '',
    redirectTo: '/upload',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
