import { ViewImage } from './../view-image.model';
import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { UploadService } from '../upload.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.css']
})
export class ImageListComponent{
  modalImage : any;
  images : ViewImage[];
  uniqueId = null;
  
  constructor(private uploadService: UploadService) {
    this.uploadService
      .getImages()
      .subscribe(
        // data => ((this.images = data.products), console.log(this.images)),
        data => (this.images = data.products, console.log(this.images)),
        // data => (console.log(data)),
        error => console.log(error)
      );
  }

  getImageId(id) {
    this.uniqueId = id;
  }
  getImageIds(){
      return this.uniqueId
  }
}
