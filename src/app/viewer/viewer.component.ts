import { ImageListComponent } from './../image-list/image-list.component';
import { UploadService } from './../upload.service';
import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, HostListener, Input } from '@angular/core';
import { ViewImage } from '../view-image.model';
import { fromEvent } from 'rxjs';
import { switchMap, takeUntil, pairwise } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.css']
})
export class ViewerComponent implements OnInit{

  ctx: CanvasRenderingContext2D;
  images : ViewImage[];
  id: any;
  ids = [];
  image : any;

  // @ViewChild('myCanvas', {static : false}) canvasRef: ElementRef;
  // @ViewChild('bubbleimg', {static : false}) Image: ElementRef;

  // @Input() public width = 400;
  // @Input() public height = 400;

  @Input() imageList: ImageListComponent;

  constructor(private uploadService: UploadService, route: ActivatedRoute) { }
  // getImage(id) {
  //   this.uploadService
  //     .getImages()
  //     .subscribe(
  //       data => (( this.id = data.products
  //           .forEach(element => {
  //             this.ids.push(element._id);
  //           }))),
  //           error => console.log(error)
  //     );
  //     console.log(this.ids);
  //     return this.ids.slice(0).find(id)
  // }

  // // ngAfterViewInit() {
  //   onDoodle(){
  //   const canvasEl: HTMLCanvasElement = this.canvasRef.nativeElement;
  //   this.ctx = canvasEl.getContext('2d');

  //   canvasEl.width = this.width;
  //   canvasEl.height = this.height;

  //   this.ctx.lineWidth = 3;
  //   this.ctx.lineCap = 'round';
  //   this.ctx.strokeStyle = '#000';

  //   this.captureEvents(canvasEl);
  //   }

  //   private captureEvents(canvasEl){
  //     fromEvent(canvasEl, 'mousedown')
  //       .pipe(
  //         switchMap((e) => {
  //           return fromEvent(canvasEl, 'mousemove')
  //           .pipe(
  //             takeUntil(fromEvent(canvasEl,'mouseup')),
  //             takeUntil(fromEvent(canvasEl, 'mouseleave')),
  //             pairwise()
  //           )
  //         })
  //       )
  //       .subscribe((res: [MouseEvent, MouseEvent]) => {
  //         const rect = canvasEl.getBoundingClientRect();

  //         const prevPos = {
  //           x: res[0].clientX - rect.left,
  //           y: res[0].clientY - rect.top
  //         };

  //         const currentPos = {
  //           x: res[1].clientX - rect.left,
  //           y: res[1].clientY - rect.top
  //         };

  //         this.drawOnCanvas(prevPos, currentPos);
    
  //       });
  //   }

  //   private drawOnCanvas(prevPos: { x: number, y: number },
  //      currentPos: { x: number, y: number }) {
  //        if(!this.ctx) {
  //          return;
  //        }
  //        this.ctx.beginPath();

  //        if(prevPos) {
  //         this.ctx.moveTo(prevPos.x, prevPos.y);
  //         this.ctx.lineTo(currentPos.x, currentPos.y);
  //         this.ctx.stroke();
  //        }
  //      }
  //   Save() {
  //     this.uploadService.saveNewImage();
  //   }

  //   Delete(id) {
  //     this.uploadService.deleteImage(id);
  //   }


  ngOnInit() {
    this.image = this.uploadService.getImages().pipe  
    this.uploadService
      .getImages()
      .subscribe(
        data => (( this.id = data.products
            .forEach(element => {
              this.ids.push(element._id);
            }))),
            error => console.log(error)
      );
      console.log(this.ids);
      // if(this.imageList.getImageIds()){
        console.log(this.imageList.getImageIds());
      // }

      // return this.ids.slice(0).find(this.imageList.uniqueId); 
    // this.image = this.uploadService.getImages().pipe  
    // this.uploadService
    //   .getImages()
    //   .subscribe(
    //     data => (( this.id = data.products
    //         .forEach(element => {
    //           this.ids.push(element._id);
    //         }))),
    //         error => console.log(error)
  }
}
