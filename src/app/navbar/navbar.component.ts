import { Component, OnInit } from '@angular/core';
import {
  Router,
  NavigationCancel,
  NavigationStart,
  NavigationEnd,
  Event,
  NavigationError,
} from "@angular/router";


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
