import { Component, OnInit } from '@angular/core';
import { UploadService } from '../upload.service';
import { ImageModel } from '../image-model';
import { Observable } from 'rxjs';
import { ViewImage } from '../view-image.model';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  images: Observable<ViewImage[]>;

  selectedFile: ImageModel;

  constructor(private uploadService: UploadService) {}

  fileUpload(el) {
    this.uploadService.uploadImage(el);
  }

  upload() {
    this.uploadService.uploadBtn();
  }

  ngOnInit() {
    this.images = this.uploadService.getImages();
  }
}
